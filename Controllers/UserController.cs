using System;
using Microsoft.AspNetCore.Mvc;
// using System.Web;
using Microsoft.AspNetCore.Http;
using MvcMovie.Models;

namespace MvcMovie.Controllers
{
    public class UserController : Controller
    {
        //private readonly MovieContext _context;

        // public UserController(MovieContext context)
        // {
        //     _context = context;
        // }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }
        public ActionResult Delete()
        {
            return View();
        }

        public ActionResult Details()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                ViewBag.error = "Login Failed ! Username or Password is empty !";
                return PartialView();
            }

            if (ModelState.IsValid)
            {
                Console.WriteLine("\n username nhap vao\n");
                Console.WriteLine(username);
                Console.WriteLine("\n password nhap vao\n");
                Console.WriteLine(password);
                if (username == "theanh" && password == "12345")
                {
                    Console.WriteLine("Dang nhap thanh cong!");
                    return RedirectToAction("Index", "Home");
                }
                else    // tài khoản không khớp
                {
                    Console.WriteLine("Dang nhap that bai!");
                    ViewBag.error = "Login failed";
                    return RedirectToAction("Login");
                }
            }
            return PartialView();
        }


    }
}
